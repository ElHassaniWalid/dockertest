git clone https://github.com/H1rkul2/devops-project-samples/tree/master/html/webapp/Application
cp Dockerfile devops-project-samples/html/webapp/Application/
cd devops-project-samples/html/webapp/Application/
docker build -t simplestaticsite .
docker run -d -p 80:80 simplestaticsite
